<?php
header('Content-Type: text/html; charset=UTF-8');

// вспомогательный массив для суперспособностей
$skills_labels = [
  'immortality' => 'Immortality',
  'idclip' => 'Passing Through Walls',
  'fly' => 'Fly'
];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  // если успешно сохранили данные
  if (isset($_COOKIE['save_form']) && $_COOKIE['save_form'] == 1) {
    setcookie('save_form', '', 1); // удаляем куку о сохранении
    $save_message = '<div id="save_message" class="popup_message">Data was successfully send</div>';
    
    // чтобы не было ошибки undefind var в form.php во время обведения поля красным
    $errors = array();
    $messages = array();
  } else {
    // заполняем массив с сообщениями об ошибках и удаляем куки с ошибками
    $messages = array();
    foreach (array_keys($_COOKIE) as $cookieName) {
      if (stristr($cookieName, '_error')) {
        $messages[substr($cookieName, 0)] = $_COOKIE[$cookieName];
        setcookie($cookieName, '', 1);
      }
    }

    // поля с ошибками
    $errors = array_keys($messages);

    // print('</br></br>MESSAGES</br>');
    // print_r($messages);
    // print('</br></br>ERRORS</br>');
    // print_r($errors);
  }

  // массив с успешно сохраненными данными для автозаполнения
  $values = array();
  foreach (array_keys($_COOKIE) as $cookieName) {
    if (stristr($cookieName, '_value')) {
      $values[substr($cookieName, 0, -6)] = $_COOKIE[$cookieName];
    }
  }
  
  // print('</br></br>VALUES</br>');
  // print_r($values);

  // выводим форму
  include('form.php');
} else {
  // VALIDATON
  $errors = false;

  // NAME
  if (empty($_POST['name'])) {
    setcookie('name_error', 'Fill the "Name"');
    $errors = true;
  } else if (!preg_match('/^[a-zA-Z, а-яА-Я]+$/u', $_POST['name'])) {
    setcookie('name_error', 'Use only Latin and Cyrillic characters');
    $errors = true;
  } else {
    // если корректно ввели, то сохраняем на год, чтобы потом вставлять, как базовое значение
    setcookie('name_value', $_POST['name'], time() + 365 * 24 * 60 * 60);
  }

  // EMAIL
  if (empty($_POST['email'])) {
    setcookie('email_error', 'Fill the "Email"');
    $errors = true;
  } else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    setcookie('email_error', 'Uncorrect Email');
    $errors = true;
  } else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
  }

  // BIRTHDAY
  if (empty($_POST['birthday'])) {
    setcookie('birthday_error', 'Pick your birthday');
    $errors = true;
  } else if (!(is_numeric($_POST['birthday']) && intval($_POST['birthday'] >= 1900 && intval($_POST['birthday']) <= 2020))) {
    setcookie('birthday_error', 'Birthday must be in interval 1900-2020');
    $errors = true;
  } else {
    setcookie('birthday_value', $_POST['birthday'], time() + 365 * 24 * 60 * 60);
  }

  // SEX
  if (empty($_POST['sex'])) {
    setcookie('sex_error', 'Pick your sex');
    $errors = true;
  } else if (!($_POST['sex'] === 'm' || $_POST['sex'] === 'f')) {
    setcookie('sex_error', 'Pick correct sex (m or f)');
    $errors = true;
  } else {
    setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
  }

  // LIMBS
  if (empty($_POST['limbs'])) {
    setcookie('limbs_error', 'Pick number of your limbs');
    $errors = true;
  } else if (!(is_numeric($_POST['limbs']) && intval($_POST['limbs']) >= 0 && intval($_POST['limbs']) <= 4)) {
    setcookie('limbs_error', 'Number of limbs must be in interval 0-4');
    $errors = true;
  } else {
    setcookie('limbs_value', $_POST['limbs'], time() + 365 * 24 * 60 * 60);
  }

  // SKILLS
  $skills_id = array_keys($skills_labels); // ['immortality','idclip', 'fly']
  if (empty($_POST['skills'])) {
    setcookie('skills_error', 'Pick skill(s)');
    $errors = true;
  } else {
    foreach ($_POST['skills'] as $skill) {
      if (!in_array($skill, $skills_id)) {
        setcookie('skills_error', 'Uncorrect skill: ' . $skill);
        $errors = true;
      } else {
        setcookie($skill . '_value', 1, time() + 365 * 24 * 60 * 60);
      }
    }
  }
  foreach ($skills_id as $skill) {
    $skill_insert[$skill] = in_array($skill, $_POST['skills']) ? 1 : 0;
  }

  // BIOGRAPHY
  if (strlen($_POST['biography']) < 200) {
    setcookie('biography_error', 'Biography have to include minimum 200 symbols');
    $errors = true;
  } else {
    setcookie('biography_value', $_POST['biography'], time() + 365 * 24 * 60 * 60);
  }

  // CONTRACT ACCEPT
  if (empty($_POST['contract_accept']) || $_POST['contract_accept'] != true) {
    setcookie('contract_accept_error', 'You must to accept the contract');
    $errors = true;
  }

  if ($errors) {
    // если есть ошибки, то перезагружаем страницу(там покажем сообщения об ошибках)
    setcookie('save_form', 0);
    header('Location: index.php');
    exit();
  } else {
    // удаляем куки с ошибками
    foreach (array_keys($_COOKIE) as $cookieName) {
      if (stristr($cookieName, '_error')) {
        setcookie($cookieName, '', 1);
      }
    }
  }

  // сохраняем в БД
  // connect to BD
  $user = 'u20574';
  $pass = '3862115';
  try {
    $db = new PDO(
      'mysql:host=localhost;dbname=u20574',
      $user,
      $pass,
      array(PDO::ATTR_PERSISTENT => true)
    );
  } catch (PDOException $e) {
    exit($e->getMessage());
  }

  // prepared request
  try {
    $stmt = $db->prepare("INSERT INTO user SET name = ?, email = ?, birthday = ?, sex = ?, limbs = ?,  skill_immortality = ?, skill_idclip = ?, skill_fly = ?, biography = ?, contract_accept = ?");
    $stmt->execute([$_POST['name'], $_POST['email'], intval($_POST['birthday']), $_POST['sex'], intval($_POST['limbs']), $skill_insert['immortality'], $skill_insert['idclip'], $skill_insert['fly'], $_POST['biography'], intval($_POST['contract_accept'])]);
  } catch (PDOException $e) {
    exit($e->getMessage());
  }

  // кука об успешном сохранении
  setcookie('save_form', 1);

  header('Location: index.php');
}
