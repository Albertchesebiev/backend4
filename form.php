<html>

<head>
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <!-- SAVE MESSAGE -->
  <?php if (isset($save_message)) {
    print($save_message);

    // таймер для pop-up сообщений 
    print '<script>setTimeout(() => {
          document.getElementById("save_message").remove();
        }, 3000);</script>';
  }
  ?>

  <form id="user-form" method="POST" action="">
    <h4>Name</h4>
    <input name="name" type="text" <?php if (in_array('name_error', $errors)) {
                                      print 'class="error_field"';
                                    } ?> value="<?php
                                                if (isset($values['name'])) {
                                                  print $values['name'];
                                                }
                                                ?>">
    <?php
    if (in_array("name_error", array_keys($messages))) {
      print '<div class="error_field_message">' . $messages["name_error"] . '</div>';
    }
    ?>

    <h4>Email</h4>
    <input type="email" name="email" <?php if (in_array('email_error', $errors)) {
                                        print 'class="error_field"';
                                      } ?> value="<?php
                                                  if (isset($values['email'])) {
                                                    print $values['email'];
                                                  }
                                                  ?>">
    <?php
    if (in_array("email_error", array_keys($messages))) {
      print '<div class="error_field_message">' . $messages["email_error"] . '</div>';
    }
    ?>


    <div class="row">

      <!-- BIRTHDAY -->
      <div class="wrap">
        <h4>Birthday</h4>
        <select name="birthday" <?php if (in_array('birthday_error', $errors)) {
                                  print 'class="error_field"';
                                } ?>>
          <?php for ($i = 1900; $i < 2020; $i++) { ?>
            <option value="<?= $i; ?>" <?php if (isset($values['birthday']) && $i == $values['birthday']) {
                                          print 'selected';
                                        } ?>><?= $i; ?></option>
          <?php } ?>
        </select>
        <?php
        if (in_array("birthday_error", array_keys($messages))) {
          print '<div class="error_field_message">' . $messages["birthday_error"] . '</div>';
        }
        ?>
      </div>

      <!-- SEX -->
      <div class="wrap">
        <h4>Sex</h4>
        <label>
          Male
          <input type="radio" name="sex" value="m" <?php if (isset($values['sex']) && 'm' == $values['sex']) {
                                                      print 'checked';
                                                    } ?>>
        </label>
        <label>
          Female
          <input type="radio" name="sex" value="f" <?php if (isset($values['sex']) && 'f' == $values['sex']) {
                                                      print 'checked';
                                                    } ?>>
        </label>
        <?php
        if (in_array("sex_error", array_keys($messages))) {
          print '<div class="error_field_message">' . $messages["sex_error"] . '</div>';
        }
        ?>
      </div>
    </div>

    <!-- LIMBS -->
    <h4 class="limbs_header">Num of limbs</h4>
    <div class="limbs_wrap">
      <?php for ($i = 0; $i <= 4; $i++) { ?>
        <label><?= $i ?><input type="radio" name="limbs" value="<?= $i ?>" <?php if (isset($values['limbs']) && $i == $values['limbs']) {
                                                                              print 'checked';
                                                                            } ?>></label>
      <?php } ?>

    </div>
    <?php
    if (in_array("limbs_error", array_keys($messages))) {
      print '<div class="error_field_message">' . $messages["limbs_error"] . '</div>';
    }
    ?>

    <!-- SKILLS -->
    <h4>Skills</h4>
    <select name="skills[]" multiple <?php if (in_array('skills_error', $errors)) {
                                        print 'class="error_field"';
                                      } ?>>>
      <?php foreach ($skills_labels as $key => $value) { ?>
        <option value="<?= $key; ?>" <?php if (isset($_COOKIE[$key . '_value'])) {
                                        print 'selected';
                                      } ?>><?= $value; ?></option>
      <?php } ?>
    </select>
    <?php
    if (in_array("skills_error", array_keys($messages))) {
      print '<div class="error_field_message">' . $messages["skills_error"] . '</div>';
    }
    ?>

    <!-- BIOGRAPHY -->
    <h4>Biography</h4>
    <textarea name="biography" cols="30" rows="10" placeholder="Biography have to include minimum 200 symbols" <?php if (in_array('biography_error', $errors)) {
                                                                                                                  print 'class="error_field"';
                                                                                                                } ?>><?php
                                                                                                                      if (isset($values['biography'])) {
                                                                                                                        print $values['biography'];
                                                                                                                      }
                                                                                                                      ?></textarea>
    <?php
    if (in_array("biography_error", array_keys($messages))) {
      print '<div class="error_field_message">' . $messages["biography_error"] . '</div>';
    }
    ?>


    <!-- CONTRACT ACCEPT -->
    <br>
    <label class="contract_accept">
      I agree with a contract
      <input type="checkbox" name="contract_accept">
    </label>
    <?php
    if (in_array("contract_accept_error", array_keys($messages))) {
      print '<div class="error_field_message">' . $messages["contract_accept_error"] . '</div>';
    }
    ?>

    <!-- SUBMIT -->
    <input class="user-form__submit" type="submit" value="Submit" name="send">
  </form>
</body>

</html>